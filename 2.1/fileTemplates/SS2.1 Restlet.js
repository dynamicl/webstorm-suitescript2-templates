#parse("ss21.header.js")

     * @NScriptType RESTlet
     */
    var exports = {};

#if ($get == 'y')
#parse("ss21.restlet.get.js")
#end

#if ($post == 'y')
#parse("ss21.restlet.post.js")
#end

#if ($put == 'y')
#parse("ss21.restlet.put.js")
#end

#if ($delete == 'y')
#parse("ss21.restlet.delete.js")
#end

#if ($get == 'y')
    #parse("ss21.restlet.get.export.js")
#end
#if ($post == 'y')
    #parse("ss21.restlet.post.export.js")
#end
#if ($put == 'y')
    #parse("ss21.restlet.put.export.js")
#end
#if ($delete == 'y')
    #parse("ss21.restlet.delete.export.js")
#end
#parse("ss21.footer.js")
