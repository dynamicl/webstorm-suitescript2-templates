#parse("ss21.header.js")

     * @NScriptType ClientScript
     */
    var exports = {};

#if ($pageInit == 'y')
#parse("ss21.client.pageInit.js")
#end

#if ($validateField == 'y')
#parse("ss21.client.validateField.js")
#end

#if ($fieldChanged == 'y')
#parse("ss21.client.fieldChanged.js")
#end

#if ($localizationContextEnter == 'y')
#parse("ss21.client.localizationContextEnter.js")
#end

#if ($localizationContextExit == 'y')
#parse("ss21.client.localizationContextExit.js")
#end

#if ($postSourcing == 'y')
#parse("ss21.client.postSourcing.js")
#end

#if ($lineInit == 'y')
#parse("ss21.client.lineInit.js")
#end

#if ($validateLine == 'y')
#parse("ss21.client.validateLine.js")
#end

#if ($validateInsert == 'y')
#parse("ss21.client.validateInsert.js")
#end

#if ($validateDelete == 'y')
#parse("ss21.client.validateDelete.js")
#end

#if ($sublistChanged == 'y')
#parse("ss21.client.sublistChanged.js")
#end

#if ($saveRecord == 'y')
#parse("ss21.client.saveRecord.js")
#end

#if ($pageInit == 'y')
    #parse("ss21.client.pageInit.export.js")
#end
#if ($validateField == 'y')
    #parse("ss21.client.validateField.export.js")
#end
#if ($fieldChanged == 'y')
    #parse("ss21.client.fieldChanged.export.js")
#end
#if ($localizationContextEnter == 'y')
    #parse("ss21.client.localizationContextEnter.export.js")
#end
#if ($localizationContextExit == 'y')
    #parse("ss21.client.localizationContextExit.export.js")
#end
#if ($postSourcing == 'y')
    #parse("ss21.client.postSourcing.export.js")
#end
#if ($lineInit == 'y')
    #parse("ss21.client.lineInit.export.js")
#end
#if ($validateLine == 'y')
    #parse("ss21.client.validateLine.export.js")
#end
#if ($validateInsert == 'y')
    #parse("ss21.client.validateInsert.export.js")
#end
#if ($validateDelete == 'y')
    #parse("ss21.client.validateDelete.export.js")
#end
#if ($sublistChanged == 'y')
    #parse("ss21.client.sublistChanged.export.js")
#end
#if ($saveRecord == 'y')
    #parse("ss21.client.saveRecord.export.js")
#end
#parse("ss21.footer.js")
