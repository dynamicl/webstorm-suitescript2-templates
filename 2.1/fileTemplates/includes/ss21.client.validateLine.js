    /**
     * validateLine event handler; executed before a line is added to an inline editor sublist or
     * editor sublist.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {CurrentRecord} context.currentRecord - The current form record.
     * @param {string} context.sublistId - The internal ID of the sublist.
     *
     * @return {boolean} true to allow line addition; false to prevent line addition.
     */
    function validateLine(context) {
    	// TODO
        return true;
    }
