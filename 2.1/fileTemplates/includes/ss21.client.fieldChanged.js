    /**
     * fieldChanged event handler; executed when a field is changed by a user or client side call.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {CurrentRecord} context.currentRecord - The current form record
     * @param {string} context.sublistId - The internal ID of the sublist.
     * @param {string} context.fieldId - The internal ID of the field that was changed.
     * @param {string} [context.lineNum] - The index of the line if the field is in a sublist or
	 * 	    matrix.
     * @param {string} [context.columnNum] - The index of the column if the field is in a matrix.
     */
    function fieldChanged(context) {
    	// TODO
    }
