    /**
     * onRequest event handler
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - The incoming request object
     * @param {ServerResponse} context.response - The outgoing response object
     */
    function onRequest(context) {
        log.audit({title: `${context.request.method} request received`});

        const eventRouter = {
            #if ($get == "y")
            [https.Method.GET]: onGet,
            #end
            #if ($post == "y")
            [https.Method.POST]: onPost
            #end
        };

        try {
            (eventRouter[context.request.method])(context);
        } catch (e) {
            onError({context: context, error: e});
        }

        log.audit({title: "Request complete."});
    }
