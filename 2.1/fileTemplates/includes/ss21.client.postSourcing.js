    /**
     * postSourcing event handler; executed on transaction forms when a field that sources
     * information from another field is modified.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {CurrentRecord} context.currentRecord - The current form record
     * @param {string} context.sublistId - The internal ID of the sublist.
     * @param {string} context.fieldId - The internal ID of the field that triggered postSourcing.
     */
    function postSourcing(context) {
    	// TODO
    }
