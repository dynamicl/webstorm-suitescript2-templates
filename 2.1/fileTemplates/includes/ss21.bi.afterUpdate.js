    /**
     * afterUpdate event handler; executes after a bundle in a target account is updated.
     *
     * @gov XXX
     *
     * @param {Object} params
     * @param {number} params.fromVersion - The version of the bundle that was previously installed
     * @param {number} params.toVersion - The version of the bundle that was just installed
     */
    function afterUpdate(params) {
        // TODO
    }
