    /**
     * validateDelete event handler; executed when removing an existing line from an edit sublist.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {CurrentRecord} context.currentRecord - The current record the user is manipulating
     *      in the UI
     * @param {string} context.sublistId - The internal ID of the sublist.
     *
     * @return {boolean} true to allow the removal; false to prevent the removal.
     */
    function validateDelete(context) {
    	// TODO
        return true;
    }
