    /**
     * validateField event handler; executes when a field is about to be changed by a user or
     * client side call.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {CurrentRecord} context.currentRecord - The current record the user is manipulating
     *  in the UI
     * @param {string} context.sublistId - The internal ID of the sublist.
     * @param {string} context.fieldId - The internal ID of the field being validated.
     * @param {string} [context.lineNum] - The index of the line if the field is in a sublist or
     *	matrix.
     * @param {string} [context.columnNum] - The index of the column if the field is in a matrix.
     *
     * @return {boolean} true if the field is valid; false to prevent the field value from changing.
     */
    function validateField(context) {
    	// TODO
        return true;
    }
