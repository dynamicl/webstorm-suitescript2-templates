    /**
     * validateInsert event handler; executed when you insert a line into an edit sublist.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {CurrentRecord} context.currentRecord - The current form record.
     * @param {string} context.sublistId - The internal ID of the sublist.
     *
     * @return {boolean} true if the line can be inserted; false to prevent the line insertion.
     */
    function validateInsert(context) {
    	// TODO
        return true;
    }
