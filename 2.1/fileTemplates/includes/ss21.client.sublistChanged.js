    /**
     * sublistChanged event handler; executed after a sublist is inserted, removed, or edited.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {CurrentRecord} context.currentRecord - The current form record.
     * @param {string} context.sublistId - The internal ID of the sublist.
     */
    function sublistChanged(context) {
    	// TODO
    }
