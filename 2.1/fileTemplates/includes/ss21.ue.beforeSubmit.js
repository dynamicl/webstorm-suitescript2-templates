    /**
     * beforeSubmit event handler; executes prior to any write operation on the record.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {Record} context.newRecord - The new record being submitted
     * @param {Record} context.oldRecord - The old record before it was modified
     * @param {UserEventType} context.type - The action type that triggered this event
     */
    function beforeSubmit(context) {
        // TODO
    }
