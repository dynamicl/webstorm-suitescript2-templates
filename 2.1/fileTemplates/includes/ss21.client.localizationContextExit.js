    /**
     * executed when the record exits the localization context that is specified on the script
     * deployment record
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {CurrentRecord} context.currentRecord - The current form record
     * @param {string} context.locale - The list of countries that represent the new localization
     *      context.
     */
    function localizationContextExit(context) {
    	// TODO
    }
