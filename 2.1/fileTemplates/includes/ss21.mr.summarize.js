    /**
     * summarize event handler
     *
     * @gov XXX
     *
     * @param {Object} summary
     * @param {number} summary.concurrency - The maximum concurrency number when executing parallel
     *      tasks for the map/reduce script.
     * @param {Date} summary.dateCreated - The time and day when the map/reduce script began running
     * @param {InputSummary} summary.inputSummary - Holds statistics regarding the input stage.
     * @param {boolean} summary.isRestarted - Indicates whether the function has been invoked
     *      previously for the current key/value pair.
     * @param {MapSummary} summary.mapSummary - Holds statistics regarding the map stage.
     * @param {Iterator} summary.output - Iterator that provides keys and values that are saved as
     *      output during the reduce stage.
     * @param {ReduceSummary} summary.reduceSummary - Holds statistics regarding the reduce stage.
     * @param {number} summary.seconds - Total seconds elapsed when running the map/reduce script.
     * @param {number} summary.usage - Total number of usage units consumed when running the script.
     * @param {number} summary.yields - Total number of yields when running the map/reduce script.
     */
    function summarize(summary) {
        let errors = parseErrors(summary);

        // TODO

        log.audit({title: "Complete."});
    }

    /**
     * Parses errors from all stages into a single list
     *
     * @gov 0
     *
     * @param {SummaryContext} summary - Holds statistics regarding execution of the script
     *
     * @returns {Object[]} list of errors encountered while running the script
     */
    function parseErrors(summary) {
        let errors = [];

        if (summary.inputSummary.error) {
            errors.push(summary.inputSummary.error);
        }

        summary.mapSummary.errors.iterator().each((k, e) => errors.push(e));
        summary.reduceSummary.errors.iterator().each((k, e) => errors.push(e));

        return errors;
    }
