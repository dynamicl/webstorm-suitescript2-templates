    /**
     * beforeLoad event handler; executes whenever a read operation occurs on a record, and prior
     * to returning the record or page.
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {Record} context.newRecord - The new record being loaded
     * @param {UserEventType} context.type - The action type that triggered this event
     * @param {Form} context.form - The current UI form
     */
    function beforeLoad(context) {
        // TODO
    }
