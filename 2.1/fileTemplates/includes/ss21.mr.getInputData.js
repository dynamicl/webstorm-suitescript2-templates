    /**
     * getInputData event handler
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {boolean} context.isRestarted - indicates whether the current invocation of the
     *      function represents a restart
     * @param {Object} context.ObjectRef - contains input data
     * @param {string|number} context.ObjectRef.id - The internal ID or script ID of the
     *      object; e.g. a Saved Search ID
     * @param {string} context.ObjectRef.string - The object's type
     *
     * @return {*[]|Object|search.Search|ObjectRef|file.File|query.Query} Data that will be used as
     *      input for the subsequent map or reduce
     */
    function getInputData(context) {
        log.audit({title: "Start ..."});

        // TODO
        #if ($parameterized == 'y')
        let params = readParameters(runtime.getCurrentScript());

        return {
            type: "search|file",
            id: params.$paramKey
            path: ""
        };
        #end
    }
