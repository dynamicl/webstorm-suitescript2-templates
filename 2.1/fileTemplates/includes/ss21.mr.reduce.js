    /**
     * reduce event handler
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {boolean} context.isRestarted - Indicates whether the function has been invoked
     *      previously for the current key/value pair.
     * @param {number} context.executionNo - Indicates whether the current invocation of the
     *      function is the first or a subsequent invocation for the current key/value pair.
     * @param {string[]} context.errors - Holds serialized errors that were thrown during previous
     *      attempts to execute the function on the current key/value pair.
     * @param {string} context.key - The key to be processed during the stage.
     * @param {string[]} context.values - The value to be processed during the stage.
     */
    function reduce(context) {
        log.audit({title: "[reduce] Processing Key:", details: context.key});

        let values = context.values.map(JSON.parse);

        // TODO
    }
