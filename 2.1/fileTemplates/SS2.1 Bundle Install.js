#parse("ss21.header.js")

     * @NScriptType BundleInstallationScript
     *
     * @see [NetSuite Help: Bundle Installation Script Type]{@link https://system.na1.netsuite.com/app/help/helpcenter.nl?fid=section_4460460309.html}
     */
    var exports = {};

#if ($afterInstall == 'y')
#parse("ss21.bi.afterInstall.js")
#end

#if ($afterUpdate == 'y')
#parse("ss21.bi.afterUpdate.js")
#end

#if ($beforeInstall == 'y')
#parse("ss21.bi.beforeInstall.js")
#end

#if ($beforeUninstall == 'y')
#parse("ss21.bi.beforeUninstall.js")
#end

#if ($beforeUpdate == 'y')
#parse("ss21.bi.beforeUpdate.js")
#end

#if ($afterInstall == 'y')
    #parse("ss21.bi.afterInstall.export.js")
#end
#if ($afterUpdate == 'y')
    #parse("ss21.bi.afterUpdate.export.js")
#end
#if ($beforeInstall == 'y')
    #parse("ss21.bi.beforeInstall.export.js")
#end
#if ($beforeUninstall == 'y')
    #parse("ss21.bi.beforeUninstall.export.js")
#end
#if ($beforeUpdate == 'y')
    #parse("ss21.bi.beforeUpdate.export.js")
#end
#parse("ss21.footer.js")
