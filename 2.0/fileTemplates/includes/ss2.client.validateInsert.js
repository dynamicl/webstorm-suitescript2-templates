    /**
     * <code>validateInsert</code> event handler
     *
     * @gov XXX
     *
     * @param context
	 * 		{Object}
     * @param context.currentRecord
	 * 		{record} The current record the user is manipulating in the UI
     * @param context.sublistId
	 * 		{string} The internal ID of the sublist.
     *
     * @return {boolean} <code>true</code> if the line can be inserted;
     *         <code>false</code> to prevent the line insertion.
     *
     * @static
     * @function validateInsert
     */
    function validateInsert(context) {
    	// TODO
        return true;
    }
