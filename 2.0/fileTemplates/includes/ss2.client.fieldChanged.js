    /**
     * <code>fieldChanged</code> event handler
     *
     * @gov XXX
     *
     * @param context
	 * 		{Object}
     * @param context.currentRecord
	 * 		{record} The current record the user is manipulating in the UI
     * @param context.sublistId
	 * 		{string} The internal ID of the sublist.
     * @param context.fieldId
	 * 		{string} The internal ID of the field that was changed.
     * @param [context.lineNum=undefined]
	 * 		{string} The index of the line if the field is in a sublist or
	 * 			matrix.
     * @param [context.columnNum=undefined]
	 * 		{string} The index of the column if the field is in a matrix.
     *
     * @return {void}
     *
     * @static
     * @function fieldChanged
     */
    function fieldChanged(context) {
    	// TODO
    }
