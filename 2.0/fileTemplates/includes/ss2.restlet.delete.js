    /**
     * <code>delete</code> event handler
     * 
     * @gov XXX
     * 
     * @param params
	 * 		{Object} The parameters from the HTTP request URL as key-value pairs
     * 
     * @return {String|Object} Returns a String when request
     *         <code>Content-Type</code> is <code>text/plain</code>;
     *         returns an Object when request <code>Content-Type</code> is
     *         <code>application/json</code>
     * 
     * @static
     * @function _delete
     */
    function _delete(params) {
        // TODO
    }
