    /**
     * <code>onAction</code> event handler
     * 
     * @gov XXX
     *
     * @param context
	 * 		{Object}
     * @param context.newRecord
	 *		{Record} The new record with all changes. <code>save()</code> is not
	 *		permitted.
     * @param context.oldRecord
	 * 		{Record} The old record with all changes. <code>save()</code> is not
	 * 		permitted.
     * @param context.form {Form} The UI form in context
     * @param context.type {string} Event Type, such as create, edit, delete
     * @param context.workflowId {number} Internal ID of the currently executing workflow
     *
     * @return {void}
     * 
     * @static
     * @function onAction
     */
    function onAction(context) {
        // TODO
    }
