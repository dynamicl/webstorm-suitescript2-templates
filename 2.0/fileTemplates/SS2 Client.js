#parse("ss2.header.js")
     
     * @NScriptType ClientScript
     */
    var exports = {};

#if ($pageInit == 'y')
#parse("ss2.client.pageInit.js")
#end

#if ($validateField == 'y')
#parse("ss2.client.validateField.js")
#end

#if ($fieldChanged == 'y')
#parse("ss2.client.fieldChanged.js")
#end

#if ($postSourcing == 'y')
#parse("ss2.client.postSourcing.js")
#end

#if ($lineInit == 'y')
#parse("ss2.client.lineInit.js")
#end

#if ($validateLine == 'y')
#parse("ss2.client.validateLine.js")
#end

#if ($validateInsert == 'y')
#parse("ss2.client.validateInsert.js")
#end

#if ($validateDelete == 'y')
#parse("ss2.client.validateDelete.js")
#end

#if ($sublistChanged == 'y')
#parse("ss2.client.sublistChanged.js")
#end

#if ($saveRecord == 'y')
#parse("ss2.client.saveRecord.js")
#end

#if ($pageInit == 'y')
    #parse("ss2.client.pageInit.export.js")
#end
#if ($validateField == 'y')
    #parse("ss2.client.validateField.export.js")
#end
#if ($fieldChanged == 'y')
    #parse("ss2.client.fieldChanged.export.js")
#end
#if ($postSourcing == 'y')
    #parse("ss2.client.postSourcing.export.js")
#end
#if ($lineInit == 'y')
    #parse("ss2.client.lineInit.export.js")
#end
#if ($validateLine == 'y')
    #parse("ss2.client.validateLine.export.js")
#end
#if ($validateInsert == 'y')
    #parse("ss2.client.validateInsert.export.js")
#end
#if ($validateDelete == 'y')
    #parse("ss2.client.validateDelete.export.js")
#end
#if ($sublistChanged == 'y')
    #parse("ss2.client.sublistChanged.export.js")
#end
#if ($saveRecord == 'y')
    #parse("ss2.client.saveRecord.export.js")
#end
#parse("ss2.footer.js")
