#parse("ss2.header.js")
     
     * @NScriptType BundleInstallationScript
     *
     * @see [NetSuite Help: Bundle Installation Script Type]{@link https://system.na1.netsuite.com/app/help/helpcenter.nl?fid=section_4460460309.html}
     */
    var exports = {};

#if ($afterInstall == 'y')
#parse("ss2.bi.afterInstall.js")
#end

#if ($afterUpdate == 'y')
#parse("ss2.bi.afterUpdate.js")
#end

#if ($beforeInstall == 'y')
#parse("ss2.bi.beforeInstall.js")
#end

#if ($beforeUninstall == 'y')
#parse("ss2.bi.beforeUninstall.js")
#end

#if ($beforeUpdate == 'y')
#parse("ss2.bi.beforeUpdate.js")
#end

#if ($afterInstall == 'y')
    #parse("ss2.bi.afterInstall.export.js")
#end
#if ($afterUpdate == 'y')
    #parse("ss2.bi.afterUpdate.export.js")
#end
#if ($beforeInstall == 'y')
    #parse("ss2.bi.beforeInstall.export.js")
#end
#if ($beforeUninstall == 'y')
    #parse("ss2.bi.beforeUninstall.export.js")
#end
#if ($beforeUpdate == 'y')
    #parse("ss2.bi.beforeUpdate.export.js")
#end
#parse("ss2.footer.js")
